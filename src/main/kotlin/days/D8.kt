package days

import getReader
import java.io.Reader

object D8 {
    private fun processInput(input: Reader): List<String> {
        return input.readLines()
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    private fun p1(input: List<String>) {
        println("Acc value : ${runProgram(input).second}")
    }

    private fun runProgram(input: List<String>): Pair<Boolean, Long> {
        val commandChecked = input.map { Pair(false, it) }.toMutableList()
        val valMap = mutableMapOf("jmp" to 0L, "acc" to 0L, "nop" to 0L)

        while (valMap["jmp"]!! < commandChecked.size && !commandChecked[valMap["jmp"]!!.toInt()].first) {
            val cmd = commandChecked[valMap["jmp"]!!.toInt()]
            commandChecked[valMap["jmp"]!!.toInt()] = Pair(true, cmd.second)
            val split = cmd.second.split(" ")
            val dest = split[0]
            var change = split[1].toLong()
            if (dest == "jmp")
                change--
            valMap[dest] = valMap[dest]!!.plus(change)
            valMap["jmp"] = valMap["jmp"]!!+1
        }

        return (valMap["jmp"]!! < commandChecked.size) to valMap["acc"]!!
    }

    private fun p2(input: List<String>) {
        var nextUncheckedLine = 0

        var cont = true
        var acc = 0L
        while (cont) {
            var newProgram = input.toMutableList()
            while (!newProgram[nextUncheckedLine].contains("(nop|jmp)".toRegex()))
                nextUncheckedLine++

            val valToChange = newProgram[nextUncheckedLine]
            newProgram[nextUncheckedLine] = if(valToChange.contains("nop"))
                valToChange.replace("nop", "jmp")
            else
                valToChange.replace("jmp", "nop")

            nextUncheckedLine++
            val ret = runProgram(newProgram)
            cont = ret.first
            acc = ret.second
        }

        println("Acc value : $acc")
    }
}

fun main() {
    D8.main(getReader(8))
}