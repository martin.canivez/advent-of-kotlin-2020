package days

import getReader
import org.apache.commons.math3.complex.Complex
import java.io.Reader
import java.lang.RuntimeException

typealias D12InputType = List<Pair<Char, Int>>

object D12 {
    private val regex = "(\\w)(\\d+)".toRegex()

    private fun processInput(input: Reader): D12InputType {
        return input.readLines()
            .map { regex.matchEntire(it) ?: throw RuntimeException(it) }
            .map { val (action, length) = it.destructured; action[0] to length.toInt() }
    }

    private val leftDirectionChangeMap =
        mapOf(90 to Complex.I,
            180 to Complex.valueOf(-1.0),
            270 to Complex.valueOf(0.0, -1.0))

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    private fun p1(input: D12InputType) {
        var startPos = Complex.ZERO
        var startDirection = Complex.ONE

    }

    private fun p2(input: D12InputType) {
    }
}

fun main() {
    D12.main(getReader(12))
}