# Advent of Kotlin 2020
## How to compile :
You need Java 11 (and Kotlin) installed
Use the included scripts in the repo ! Grade is shipped in this
project, just use gradlew or gradlew.bat depending on your OS

## What to run :
You can run the main main function in the base module, it will run
all the days present in the code. You can also run the main functions
in the "days.DX.kt" files to run part 1 and 2 of a given day

## How to get the subjects :
https://adventofcode.com/2020

The subject for the part 2 only appears once you have finished part 1.
If you want to run the parts using your own inputs, just change the
files names "dX.txt" in the resources folder with your own input.