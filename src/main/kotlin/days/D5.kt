package days

import getReader
import java.io.Reader
import java.lang.RuntimeException

typealias D5InputType = Set<Pair<Int, Int>>

val d5regex = "(\\w{7})(\\w{3})".toRegex()

object D5 {
    private fun processInput(input: Reader): D5InputType {
        return input.readLines()
            .map { d5regex.matchEntire(it) ?: throw RuntimeException(it) }
            .map {
                val (row, seat) = it.destructured
                Pair(row, seat)
            }
            .mapTo(HashSet(), {
                var (rowInt, seatInt) = listOf(0, 0)
                for (c in it.first) {
                    rowInt = rowInt shl 1
                    rowInt += if (c == 'B') 1 else 0
                }
                for (c in it.second) {
                    seatInt = seatInt shl 1
                    seatInt += if (c == 'R') 1 else 0
                }
                Pair(rowInt, seatInt)
            })
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    fun p1(input: D5InputType) {
        println("Highest value : ${input.map { getId(it) }.maxOrNull()}")
    }

    fun p2(input: D5InputType) {
        val takenSeats = input.map { getId(it) }
        val available = (0..getId(Pair(127, 7))).filter { !takenSeats.contains(it) }
        var lastSeen = -1;

        for (c in available)
        {
            val currLastSeen = lastSeen
            lastSeen = c

            if (currLastSeen+1 != lastSeen)
                break
        }

        println("Your seat : $lastSeen")
    }

    private fun getId(seat: Pair<Int, Int>): Int {
        return seat.first*8 + seat.second
    }
}


fun main() {
    D5.main(getReader(5))
}