package days

import com.google.common.collect.ArrayTable
import com.google.common.collect.Table
import days.D11CellState.*
import getReader
import java.io.Reader
import java.lang.RuntimeException

typealias D11InputType = Table<Int, Int, D11CellState>

object D11 {
    val directions = setOf(-1 to -1, -1 to 0, -1 to 1, 0 to -1, 0 to 1, 1 to -1, 1 to 0, 1 to 1)

    private fun processInput(input: Reader): D11InputType {
        val tda = input.readLines().map {
            it.map { e -> when (e) {
                'L' -> SEAT
                '.' -> FLOOR
                else -> throw RuntimeException(e.toString())
            } }
        }

        val out = ArrayTable.create<Int, Int, D11CellState>(tda.indices, tda[0].indices)
        tda.forEachIndexed {row, list -> list.forEachIndexed{ col, e -> out.put(row, col, e) } }
        return out
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    private fun p1(input: D11InputType) {
        var oldVal = input
        var newVal = p1SeatsIteration(input)

        while (oldVal != newVal)
        {
            oldVal = newVal
            newVal = p1SeatsIteration(newVal)
        }

        println("Got ${newVal.values().filter { it == OCC_SEAT }.count()} people seated")
    }

    private fun p2(input: D11InputType) {
        var oldVal = input
        var newVal = p2SeatsIteration(input)

        while (oldVal != newVal)
        {
            oldVal = newVal
            newVal = p2SeatsIteration(newVal)
        }

        println("Got ${newVal.values().filter { it == OCC_SEAT }.count()} people seated")
    }

    private fun p1SeatsIteration(currentGen: D11InputType): D11InputType {
        return seatsIteration(currentGen, 4, p1NeighbourLogic)
    }

    private fun p2SeatsIteration(currentGen: D11InputType): D11InputType {
        return seatsIteration(currentGen, 5, p2NeighbourLogic)
    }

    private fun seatsIteration(currentGen: D11InputType,
                               occToFreeThreshold: Int,
                               neighbourLogic: D11NeighbourLogic): D11InputType {
        val newGen = ArrayTable.create<Int, Int, D11CellState>(currentGen.rowKeySet(), currentGen.columnKeySet())
        currentGen.rowMap().forEach { ri, col -> col.forEach { ci, cellVal -> newGen.put(ri, ci, when (cellVal) {
            FLOOR -> FLOOR
            SEAT -> if (neighbourLogic.getNeighbours(ri, ci, currentGen).none { it == OCC_SEAT }) OCC_SEAT else SEAT
            OCC_SEAT -> if (neighbourLogic.getNeighbours(ri, ci, currentGen).filter { it == OCC_SEAT }.count() >= occToFreeThreshold) SEAT else OCC_SEAT
        } ) } }
        return newGen
    }

    private val p1NeighbourLogic = D11NeighbourLogic { row, col, table ->
        val out1 = table.rowMap()
        val out2 = out1.filter { it.key >= row - 1 && it.key <= row + 1 }
        val out3 = out2.map { it.value.filterKeys { itCol -> itCol >= col - 1 && itCol <= col + 1 && (it.key != row || itCol != col) } }
        out3.flatMap { it.values }
    }

    private val p2NeighbourLogic = D11NeighbourLogic { row, col, table ->
        directions.mapNotNull { getNeighbourInDirection(row, col, it, table) }
    }
    //Map<Pair<Origin, Direction>, Cell>
    private val neighbourInDirectionCache = HashMap<Pair<Pair<Int, Int>, Pair<Int, Int>>, Pair<Int, Int>>()

    private fun getNeighbourInDirection(row: Int, col: Int, direction: Pair<Int, Int>, table: Table<Int, Int, D11CellState>): D11CellState? {
        if (!neighbourInDirectionCache.contains((row to col) to direction))
            neighbourInDirectionCache[(row to col) to direction] = getNeighbourCoordsInDirection(row, col, direction, table)

        val coords = neighbourInDirectionCache[(row to col) to direction]!!
        return table[coords.first, coords.second]
    }

    private fun getNeighbourCoordsInDirection(row: Int, col: Int, direction: Pair<Int, Int>, table: Table<Int, Int, D11CellState>): Pair<Int, Int> {
        var currRow = row+direction.first
        var currCol = col+direction.second

        while (table[currRow, currCol] == FLOOR) {
            currRow += direction.first
            currCol += direction.second
        }

        return currRow to currCol
    }
}

fun interface D11NeighbourLogic {
    fun getNeighbours(row: Int, col: Int, table: Table<Int, Int, D11CellState>): List<D11CellState>
}

enum class D11CellState {
    FLOOR,
    SEAT,
    OCC_SEAT
}

fun main() {
    D11.main(getReader(11))
}