package days

import getReader
import java.io.Reader

object D1 {
    fun processInput(input: Reader): Set<Int> {
        return HashSet(input.readLines().map { it.toInt() })
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    private fun p1(processed: Set<Int>) {
        for (i in processed)
        {
            if (processed.contains(2020-i)) {
                println("Got pair $i-${2020 - i}")
                println("Result : ${i * (2020 - i)}")
                return
            }
        }
    }

    private fun p2(processed: Set<Int>) {
        val smallest = processed.minOrNull()!!
        for (i in processed) {
            val remaining = processed.filterTo(HashSet(), { it < 2020 - i - smallest })
            for (j in remaining) {
                if (remaining.contains(2020 - i - j)) {
                    println("Found triplet $i-$j-${2020-i-j}")
                    println("Result : ${i*j*(2020-i-j)}")
                    return
                }
            }
        }
    }
}


fun main() {
    D1.main(getReader(1))
}