package days

import getReader
import java.io.Reader
import java.lang.RuntimeException

typealias D4InputType = Set<Passport>

object D4 {
    private fun processInput(input: Reader): D4InputType {
        val processed = input.readLines().map { it.trim() }

        val passports = HashSet<Passport>()
        var currPassport = Passport()

        for (line in processed) {
            if (line == "") {
                passports.add(currPassport)
                currPassport = Passport()
            } else {
                val attrs = line.split(" ")
                for (attr in attrs) {
                    val (key, value) = attr.split(":")
                    when (key) {
                        "byr" -> currPassport.byr = value
                        "iyr" -> currPassport.iyr = value
                        "eyr" -> currPassport.eyr = value
                        "hgt" -> currPassport.hgt = value
                        "hcl" -> currPassport.hcl = value
                        "ecl" -> currPassport.ecl = value
                        "pid" -> currPassport.pid = value
                        "cid" -> currPassport.cid = value
                        else -> throw RuntimeException("$key invalid")
                    }
                }
            }
        }
        passports.add(currPassport)
        return passports;
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    fun p1(input: D4InputType) {
        println("Passports valid: ${input.filter { it.isP1valid() }.count()}")
    }

    fun p2(input: D4InputType) {
        println("Passports valid: ${input.filter { it.isP2valid() }.count()}")
    }
}

val hgtRegex = "(\\d+)(cm|in)".toRegex()
val hclRegex = "#[0-9a-fA-F]{6}".toRegex()
val pidRegex = "[0-9]{9}".toRegex()

class Passport {
    var byr: String? = null
    var iyr: String? = null
    var eyr: String? = null
    var hgt: String? = null
    var hcl: String? = null
    var ecl: String? = null
    var pid: String? = null
    var cid: String? = null

    fun isP1valid(): Boolean {
        return byr != null &&
                iyr != null &&
                eyr != null &&
                hgt != null &&
                hcl != null &&
                ecl != null &&
                pid != null
    }

    fun isP2valid(): Boolean {
        if (!isP1valid()) return false

        val parsedByr = byr!!.toIntOrNull() ?: return false
        if (parsedByr < 1920 || parsedByr > 2002) return false

        val parsedIyr = iyr!!.toIntOrNull() ?: return false
        if (parsedIyr < 2010 || parsedIyr > 2020) return false

        val parsedEyr = eyr!!.toIntOrNull() ?: return false
        if (parsedEyr < 2020 || parsedEyr > 2030) return false

        val hgtMatch = hgtRegex.matchEntire(hgt!!) ?: return false
        val (hgtValStr, hgtUnit) = hgtMatch.destructured
        val hgtVal = hgtValStr.toIntOrNull() ?: return false
        when (hgtUnit) {
            "cm" -> if (hgtVal < 150 || hgtVal > 193) return false
            "in" -> if (hgtVal < 59 || hgtVal > 76) return false
            else -> return false
        }

        hclRegex.matchEntire(hcl!!) ?: return false

        if (ecl!! !in setOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")) return false

        pidRegex.matchEntire(pid!!) ?: return false

        return true
    }
}

fun main() {
    D4.main(getReader(4))
}