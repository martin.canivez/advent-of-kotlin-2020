package days

import getReader
import java.io.Reader
import java.lang.RuntimeException

typealias D7InputType = Set<Bag>

data class ContainingDef(val amount: Int, val color: String)

data class Bag(val color: String, val contains: Set<ContainingDef>)

val d7Regex = "(.*) bags contain (.+)\\.".toRegex()
val d7includedRegex = "(\\d+) (.*) bags?".toRegex()
val noBagsConst = "no other bags";

object D7 {
    private fun processInput(input: Reader): D7InputType {
        return input.readLines()
            .map { d7Regex.matchEntire(it) ?: throw RuntimeException(it) }
            .map {
                val (container, containingStr) = it.destructured
                val containing = if (containingStr == noBagsConst) {
                    emptySet()
                }
                else {
                    containingStr.split(", ")
                        .map { d7includedRegex.matchEntire(it) ?: throw RuntimeException(it) }
                        .map { ContainingDef(it.destructured.component1().toInt(), it.destructured.component2()) }.toSet()

                }
                Bag(container, containing)
            }.toSet()
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    private fun p1(input: D7InputType) {
        var nextToCheck = setOf("shiny gold")
        val solutions = HashSet<String>()

        while (nextToCheck.size > 0) {
            val bagsFitting = input.filter { it.contains.any { e -> nextToCheck.contains(e.color) } }
            nextToCheck = bagsFitting.map { it.color }.toSet()
            solutions.addAll(nextToCheck)
        }

        println("Amount of bags containing shiny gold: ${solutions.size}")
    }

    private fun p2(input: D7InputType) {
        val shinyBag = input.find { it.color == "shiny gold" }!!
        println("You need ${countBagsInsideAndThisOne(input, shinyBag)-1} bags")
    }

    private fun countBagsInsideAndThisOne(input: D7InputType, bag: Bag): Int {
        return bag.contains.map { it.amount * countBagsInsideAndThisOne(input, input.find { e -> e.color == it.color }!!) }.sum() + 1
    }
}

fun main() {
    D7.main(getReader(7))
}