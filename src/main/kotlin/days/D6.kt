package days

import getReader
import java.io.Reader

typealias D6InputType = MutableList<MutableList<Set<Char>>>

object D6 {
    private fun processInput(input: Reader): D6InputType {
        return input.readLines()
            .fold(ArrayList(mutableListOf(ArrayList())),
                { arrayList, s ->
                    when (s) {
                        "" -> arrayList.add(ArrayList())
                        else -> arrayList.last().add(s.toCharArray()
                            .fold(HashSet(), {set, c -> set.add(c); set }))
                    }
                    arrayList
                })
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    private fun p1(input: D6InputType)
    {
        println("Total amount of at least one yes : ${input.map { it.fold(HashSet<Char>(), { s1, s2 -> s1.addAll(s2); s1}).size}.sum()}")
    }

    private fun p2(input: D6InputType)
    {
        println("Total amount of all yes in a family : ${input.map { it.fold(HashSet(('a'..'z').toSet()), { s1, s2 -> s1.retainAll(s2); s1}).size}.sum()}")
    }
}

fun main() {
    D6.main(getReader(6))
}