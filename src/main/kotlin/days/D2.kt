package days

import getReader
import java.io.Reader

data class PasswordCol(val charRule: Char, val min: Int, val max: Int, val password: String)

val d2LineRegex = "(\\d+)-(\\d+) (\\w): (\\w+)".toRegex()

object D2 {
    private fun processInput(input: Reader): List<PasswordCol> {
        return input.readLines().map {
            val result = d2LineRegex.matchEntire(it);
            val (min, max, letter, password) = result!!.destructured
            PasswordCol(letter[0], min.toInt(), max.toInt(), password)
        }
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    fun p1(input: List<PasswordCol>) {
        val valid = input.count {
            val amount = it.password.count { e -> e == it.charRule }
            amount >= it.min && amount <= it.max
        }
        println("$valid passwords are valid")
    }

    fun p2(input: List<PasswordCol>) {
        val valid = input.count {
            (it.password[it.min-1] == it.charRule) xor
                    (it.password.length >= it.max && it.password[it.max-1] == it.charRule) }
        println("$valid passwords are valid")
    }
}

fun main() {
    D2.main(getReader(2))
}