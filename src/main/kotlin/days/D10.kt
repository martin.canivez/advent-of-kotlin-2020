package days

import getReader
import java.io.Reader

object D10 {
    val p2SolutionCache = HashMap<Int, Long>()

    private fun processInput(input: Reader): List<Int> {
        val out = input.readLines().map(String::toInt).sorted().toMutableList()
        out.add(out.maxOrNull()!!+3)
        return out
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    private fun p1(input: List<Int>) {
        val joltDiffMap = mutableMapOf(1 to 0, 3 to 0)
        var lastAdapterValue = 0

        for (currAdapter in input) {
            joltDiffMap[currAdapter-lastAdapterValue] = joltDiffMap[currAdapter-lastAdapterValue]!! + 1
            lastAdapterValue = currAdapter
        }

        println("Result : ${joltDiffMap[1]!! * joltDiffMap[3]!!}")
    }

    private fun p2(input: List<Int>) {
        println("${numberOfSolutionFrom(0, input)} possible solutions")
    }

    private fun cachedNumberOfSolutionsFrom(currentRating: Int, input: List<Int>): Long {
        if (p2SolutionCache.containsKey(currentRating))
            return p2SolutionCache[currentRating]!!

        val out = numberOfSolutionFrom(currentRating, input)
        p2SolutionCache[currentRating] = out
        return out
    }

    private fun numberOfSolutionFrom(currentRating: Int, input: List<Int>): Long {
        if (currentRating == input.last())
            return 1

        val nextValsToCheck = input.filter { it >= currentRating+1 && it <= currentRating+3 }
        return nextValsToCheck.map { cachedNumberOfSolutionsFrom(it, input) }.sum()
    }
}


fun main() {
    D10.main(getReader(10))
}