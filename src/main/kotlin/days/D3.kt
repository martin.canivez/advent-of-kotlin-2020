package days

import com.google.common.collect.ArrayTable
import com.google.common.collect.Table
import getReader
import java.io.Reader
import java.lang.RuntimeException

typealias D3InputType = Table<Int, Int, Boolean>

object D3 {
    private fun processInput(input: Reader): D3InputType {
        val tda = input.readLines().map {
            it.map { e -> when (e) {
                '.' -> false
                '#' -> true
                else -> throw RuntimeException(e.toString())
            } }
        }

        val out = ArrayTable.create<Int, Int, Boolean>(tda.indices, tda[0].indices)
        tda.forEachIndexed {row, list -> list.forEachIndexed{ col, e -> out.put(row, col, e) } }
        return out
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    fun p1(input: D3InputType) {
        traverse(input, 3, 1)
    }

    fun p2(input: D3InputType) {
        val tries = listOf(Pair(1, 1), Pair(3, 1), Pair(5, 1), Pair(7, 1), Pair(1, 2));
        var result = 1L

        for (currentTry in tries) {
            println("For try $currentTry")
            result *= traverse(input, currentTry.first, currentTry.second)
        }

        println("Result: $result")
    }

    private fun traverse(input: D3InputType, rightStep: Int, downStep: Int): Int {
        var treesHit = 0
        var currentWidth = 0
        for (currentHeight in downStep until input.rowKeySet().size step downStep) {
            currentWidth = (currentWidth+rightStep)%input.columnKeySet().size
            treesHit += if (input[currentHeight, currentWidth] == true) 1 else 0
        }

        println("Hit $treesHit trees")
        return treesHit
    }
}

fun main() {
    D3.main(getReader(3))
}