import java.io.InputStreamReader
import java.io.Reader
import java.lang.reflect.InvocationTargetException
import java.time.LocalDate
import java.time.ZoneId
import kotlin.Exception

fun main() {
    println("Welcome to the advent of code !")
    for (day in 1..25) {
        try {
            val cname = "days.D${day}Kt"
            val k = Class.forName(cname)
            val m = k.getMethod("main")
            println("Day $day")
            m.invoke(k)
            println("\n\n----------------------------------------------------\n\n")
        } catch (e: Exception) {
            errorHandling(day, e)
        }
    }
}

fun getReader(day: Int): Reader {
    val inputResource = {}.javaClass.getResource("d${day}.txt")
    return InputStreamReader(inputResource.openStream())
}

fun errorHandling(day: Int, e: Exception) {

    val message = when (e) {
        is ClassNotFoundException -> {
            val date = LocalDate.now(ZoneId.of("EST", ZoneId.SHORT_IDS));
            val unlockTime = LocalDate.parse("2020-12-${String.format("%02d", day)}");

            if (!date.isBefore(unlockTime)) "No class found even though challenge is available" else null
        }
        is NoSuchMethodException -> "No main method"
        is IllegalAccessException -> "Main can't be accessed"
        is InvocationTargetException -> "Wrong main invocation target"
        else -> e.message
    }

    if (message != null)
        println("Issues for day $day : $message")
}