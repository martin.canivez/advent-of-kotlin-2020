import java.io.Reader
import java.lang.RuntimeException


object D9 {
    val preambleSize = 25;

    private fun processInput(input: Reader): List<Long> {
        return input.readLines().map(String::toLong)
    }

    fun main(input: Reader) {
        val processed = processInput(input)
        println("Part 1\n")
        p1(processed)
        println("\nPart 2\n")
        p2(processed)
    }

    private fun p1(input: List<Long>) {
        val indexOfVal = findIndexOfErroneousValue(input)
        println("${input[indexOfVal]} is not a sum of 2 of the 25 previous numbers")
    }

    private fun p2(input: List<Long>) {
        val indexOfVal = findIndexOfErroneousValue(input)
        val targetVal = input[indexOfVal]

        for (i in input.indices) {
            var currentMaxIndex = i
            var acc = input[i]
            while (acc < targetVal && currentMaxIndex < input.size) {
                currentMaxIndex++
                acc += input[currentMaxIndex]
            }

            if (acc == targetVal) {
                val addList = input.subList(i, currentMaxIndex+1)
                println("Array found. Result : ${addList.minOrNull()!! + addList.maxOrNull()!!}")
                return
            }
        }
    }

    private fun findIndexOfErroneousValue(input: List<Long>) : Int {
        for (i in 0 until input.size - preambleSize) {
            val subList = input.subList(i, i + preambleSize)
            val valToFind = input[i + preambleSize]

            if (!listContainsSumToNumber(subList, valToFind))
                return i + preambleSize
        }

        throw RuntimeException()
    }

    private fun listContainsSumToNumber(list: List<Long>, number: Long): Boolean {
        for (j in list)
        {
            if (list.contains(number-j))
                return true
        }
        return false
    }
}

fun main() {
    D9.main(getReader(9))
}